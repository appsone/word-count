package com.appnomic.pipeline;

import com.appnomic.flink.utility.SerializerDeserializer;
import com.appnomic.flink.utility.sink.DurableRMQSink;
import com.appnomic.flink.utility.sink.CouchBaseSink;
import com.appnomic.flink.utility.window.KeyedWindowAssigner;
import com.appnomic.objects.Data;
import com.appnomic.objects.OutPutData;
import com.appnomic.objects.TimeAmplifyNewData;
import com.appnomic.pipeline.spec.aggregator.FoldAggregator;
import com.appnomic.pipeline.spec.*;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.common.functions.FoldFunction;
import org.apache.flink.streaming.api.CheckpointingMode;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.connectors.rabbitmq.RMQSource;
import org.apache.flink.streaming.connectors.rabbitmq.common.RMQConnectionConfig;
import org.apache.flink.util.Collector;
import java.util.List;
import java.util.Map;
import com.appnomic.pipeline.spec.checkpoint.*;
import com.appnomic.flink.utility.util.PipelineException;
import com.appnomic.flink.utility.function.CheckpointedFlatMapFunc;
import com.appnomic.flink.utility.function.CheckpointedFoldFunc;
import org.apache.flink.api.common.functions.RichFlatMapFunction;
import org.apache.flink.api.java.utils.ParameterTool;


public class WordCountPipeline {

    public static void main(String[] args) throws Exception{

        Configurator configurator = new com.appnomic.configurator.Configuration();

        //Initialize environment variable
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);

        //Init received arguments
        ParameterTool parameters = ParameterTool.fromArgs(args);
        Map<String, String> parmMap = parameters.toMap();

        configurator.init(parmMap);
        //Converting updated map to ParameterTool
        parameters = ParameterTool.fromMap(parmMap);
        env.getConfig().setGlobalJobParameters(parameters);

        //Register all custom classes with its serializer
        //registerCustomTypesWithKryo(env);

        //Read from configuration
        long checkpointInterval = configurator.checkpointInterval();
        checkpointInterval = checkpointInterval > 0 ? checkpointInterval : 300000;

        long checkpointTimeoutInterval = configurator.checkpointTimeoutInterval();
        checkpointTimeoutInterval = checkpointTimeoutInterval > 0 ? checkpointTimeoutInterval : 120000;

        if (checkpointInterval > 0) {
            env.enableCheckpointing(checkpointInterval * 1000);
            env.getCheckpointConfig().setCheckpointingMode(CheckpointingMode.AT_LEAST_ONCE);
            env.getCheckpointConfig().setMaxConcurrentCheckpoints(1);
            env.getCheckpointConfig().setCheckpointTimeout(checkpointTimeoutInterval *1000);
        }

        //RMQ configuration
        String mqServerIP = configurator.rmqIP();
        int mqServerPort = configurator.rmqPort();
        String mqServerUsername = configurator.rmqUser();
        String mqServerPassword = configurator.rmqPass();
        if (mqServerIP == null || mqServerIP.isEmpty()) {
            throw new PipelineException("MQ Server IP not set in the configuration file");
        }
        RMQConnectionConfig rmqConnectionConfig = new RMQConnectionConfig.Builder()
                .setHost(mqServerIP)
                .setPort(mqServerPort)
                .setUserName(mqServerUsername)
                .setPassword(mqServerPassword)
                .setVirtualHost("/")
                .build();

        //adding RMQ Source
        DataStreamSource<Data> rmqSource = (DataStreamSource<Data>) env
                .addSource(new RMQSource<>(rmqConnectionConfig, configurator.inputQueue(),  new SerializerDeserializer<Data>(Data.class)))
                .name("adding RMQ SOurce");


        //A flink pipeline for word count
        DataStream<OutPutData> output = rmqSource
             //{
              .keyBy(d ->d.getStr())
              //}
            //{
             .flatMap(new RichFlatMapFunction<Data, Data>() {
              Filter<Data> filterClass = new com.appnomic.funtions.FillerFilter("in", "The", "of");
            //}
                @Override
                public void flatMap(Data in, Collector<Data> collector) throws PipelineException {
                    if (!filterClass.filter(in)) collector.collect(in);
                }

                @Override
                public void open(org.apache.flink.configuration.Configuration parameters) throws Exception {
                    ParameterTool parameterTool = (ParameterTool) getRuntimeContext().getExecutionConfig().getGlobalJobParameters();
                    Map<String, String> paramMap = parameterTool.toMap();
                    filterClass.init(paramMap);
                }
            }).name("FilterFillers")
            //             //            //{
            .flatMap(new RichFlatMapFunction<Data, Data>() {
                Transformer<Data, Data> transformer = new com.appnomic.funtions.WordTransformer("tolower");
             //}
                @Override
                public void flatMap(Data in, Collector<Data> collector) throws PipelineException {
                   Data transformed = transformer.transform(in);
                   if(transformed!=null)
                     collector.collect(transformed);
                }

                @Override
                public void open(org.apache.flink.configuration.Configuration parameters) throws Exception {
                   ParameterTool parameterTool = (ParameterTool) getRuntimeContext().getExecutionConfig().getGlobalJobParameters();
                   Map<String, String> paramMap = parameterTool.toMap();
                   transformer.init(paramMap);
                }
            }).name("WordTransformer")
            //              //{
              .keyBy(d ->d.getStr())
              //}
             //{
             .flatMap(new CheckpointedFlatMapFunc<Data,KeyValue<String, TimeAmplifyNewData>, Data>() {
                CheckPointKVTrnasformer<Data, String, TimeAmplifyNewData, Data> kvTransformer = new com.appnomic.funtions.CPKVImplTransformer();

                @Override
                public List<Data> snapshotState(long checkpointId, long timestamp) throws PipelineException {
                    return kvTransformer.snapshotState();
                }
                @Override
                public void restoreState(List<Data> state) throws PipelineException {
                     kvTransformer.restoreState(state);
                }
                //}

               @Override
               public void flatMap(Data in, Collector<KeyValue<String, TimeAmplifyNewData>> collector) throws PipelineException {
                        KeyValue<String, TimeAmplifyNewData> transformed = kvTransformer.transform(in);
                        if(transformed!=null)
                            collector.collect(transformed);
                }
              @Override
              public void open(org.apache.flink.configuration.Configuration parameters) throws Exception {
                 ParameterTool parameterTool = (ParameterTool) getRuntimeContext().getExecutionConfig().getGlobalJobParameters();
                 Map<String, String> paramMap = parameterTool.toMap();
                 kvTransformer.init(paramMap);
               }

             }).name("KVTransform")
              //{
              .uid("KVTransform")
              //}

            .keyBy(data -> data.key())
            //Data will be emitted once outOfrder Time is over , even if no more data is coming.
            .window(new KeyedWindowAssigner<>(k -> k.key(), (p, q) -> p.value().getTimestamp() * 1000, Time.seconds(0), Time.seconds(0), Time.seconds(0 + 60)))
            .fold(new OutPutData(0, ""), new FoldFunction<KeyValue<String, TimeAmplifyNewData>, OutPutData>() {
            FoldAggregator<OutPutData, KeyValue<String, TimeAmplifyNewData>> folder = new com.appnomic.funtions.KVImplReducer();

              @Override
              public OutPutData fold(OutPutData accumulator, KeyValue<String, TimeAmplifyNewData> keyValue) throws PipelineException {
                  return folder.aggregate(accumulator, keyValue);
              }
            }).name("KVReduce")

           ;

            //Write to sink(s)

            output
                .addSink(new DurableRMQSink<>(rmqConnectionConfig, "q", new SerializerDeserializer<OutPutData>(OutPutData.class))).name("RMQ-Sink");

            output
                .flatMap(new FlatMapFunction<OutPutData, KeyValue<String,OutPutData>>() {
                  Transformer<OutPutData, KeyValue<String,OutPutData>> transformer = new com.appnomic.funtions.CouchBaseKeyValueTransformer();
                    @Override
                    public void flatMap(OutPutData in, Collector<KeyValue<String,OutPutData>> collector) throws PipelineException {
                       KeyValue<String,OutPutData> transformed = transformer.transform(in);
                       if(transformed!=null)
                         collector.collect(transformed);
                    }
                }).name("couchBaseTrnasformer")
                .addSink(new CouchBaseSink(configurator.couchBaseNodes(), configurator.couchBaseBucket(), configurator.couchBaseBucketPass(), configurator.couchBaseDocumentTTL())).name("CouchBase-Sink");

            env.execute();
    }
}