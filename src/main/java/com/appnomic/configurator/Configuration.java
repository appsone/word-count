package com.appnomic.configurator;

/*
 *
 * Created by Akanksha on 9/8/18
 *
 */


import com.appnomic.pipeline.spec.Configurator;

import java.util.Map;

public class Configuration implements Configurator {
    @Override
    public void init(Map<String, String> map) throws Exception {

    }

    @Override
    public String rmqIP() {
        return "192.168.13.96";
    }


    @Override
    public int rmqPort() {
        return 5672;
    }

    @Override
    public String rmqPass() {
        return "guest";
    }

    @Override
    public String rmqUser() {
        return "guest";
    }
    @Override
    public long checkpointInterval() {
        return 60;
    }

    @Override
    public long checkpointTimeoutInterval() {
        return 0;
    }

    @Override
    public String inputQueue() {
        return "inputQueue";
    }

    @Override
    public String outputQueue() {
        return "outPutQueue";
    }

    @Override
    public String couchBaseNodes() {
        return "192.168.13.131:8091";
    }

    @Override
    public String couchBaseBucket() {
        return "tfp";
    }

    @Override
    public String couchBaseBucketPass() {
        return "";
    }

    @Override
    public int couchBaseDocumentTTL() {
        return 36000;
    }
}
