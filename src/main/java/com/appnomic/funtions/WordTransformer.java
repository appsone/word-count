package com.appnomic.funtions;

/*
 *
 * Created by Akanksha on 13/8/18
 *
 */

import com.appnomic.objects.Data;
import com.appnomic.pipeline.spec.Transformer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Locale;
import java.util.Map;


/*
 This calss transform Data into Dummy object and checkpoint Map Method
 */

public class WordTransformer implements Transformer<Data, Data> {

    private static final Logger log = LoggerFactory.getLogger(WordTransformer.class);

    private String conversionCase;

    public WordTransformer(String strCase){
        conversionCase = strCase;

    }

    @Override
    public Data transform(Data data) {

        String caseString;
        Data newData = null;

        if(conversionCase.equalsIgnoreCase("tolower")){
            //Transform Data to another object
            caseString = data.getStr().toLowerCase(Locale.getDefault());
        }else{
            caseString = data.getStr().toUpperCase(Locale.getDefault());
        }

        if(!caseString.isEmpty()) {
            newData = new Data(caseString, data.getTimeStamp());
        }

        log.debug("After Transformation , Data :\n{} ", newData);
        return newData;
    }

    @Override
    public void init(Map<String, String> map) throws Exception {

    }
}
