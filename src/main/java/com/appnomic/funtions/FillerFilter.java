package com.appnomic.funtions;

/*
 *
 * Created by Akanksha on 14/8/18
 *
 */


import com.appnomic.objects.Data;
import com.appnomic.pipeline.spec.Filter;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/*
    Filter Filler Data String
    Input : Data
    Output : Data
 */
public class FillerFilter implements Filter<Data> {

    private List<String> stopWordList = new ArrayList<String>();

   /* public FillerFilter(String[] stopWord){
        for (String word :stopWord) {
            stopWordList.add(word);
        }


    }*/

    public FillerFilter(String str1, String str2, String str3){
            stopWordList.add(str1);
            stopWordList.add(str2);
            stopWordList.add(str3);
    }


    @Override
    public boolean filter(Data o) {
        return null == o || o.getStr().isEmpty()|| stopWordList.contains(o.getStr());

    }

    @Override
    public void init(Map<String, String> map) throws Exception {

    }
}
