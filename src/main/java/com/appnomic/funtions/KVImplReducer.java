package com.appnomic.funtions;


import com.appnomic.objects.OutPutData;
import com.appnomic.objects.TimeAmplifyNewData;
import com.appnomic.pipeline.spec.KeyValue;
import com.appnomic.pipeline.spec.aggregator.FoldAggregator;


public class KVImplReducer implements FoldAggregator<OutPutData, KeyValue<String, TimeAmplifyNewData>> {


    @Override
    public OutPutData aggregate(OutPutData acc, KeyValue<String, TimeAmplifyNewData> keyvalue) {
        acc.setCount(acc.getCount()+1);
        acc.setValue(acc.getValue() + keyvalue.value().getValue());
        if(acc.getStr().isEmpty()){
            acc.setStr(keyvalue.key());
        }
        return acc;
    }
}
