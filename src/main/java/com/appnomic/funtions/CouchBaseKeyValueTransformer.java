package com.appnomic.funtions;

/*
 *
 * Created by Akanksha on 16/8/18
 *
 */


import com.appnomic.objects.OutPutData;
import com.appnomic.pipeline.spec.KeyValue;
import com.appnomic.pipeline.spec.Transformer;

import java.util.Map;

public class CouchBaseKeyValueTransformer implements Transformer<OutPutData, KeyValue<String, OutPutData>> {


    @Override
    public KeyValue<String, OutPutData> transform(OutPutData outPutData) {
        return KeyValue.of(outPutData.getStr(), outPutData);
    }

    @Override
    public void init(Map<String, String> map) throws Exception {

    }
}
