package com.appnomic.funtions;

/*
 *
 * Created by Akanksha on 10/8/18
 *
 */


import com.appnomic.objects.Data;
import com.appnomic.objects.TimeAmplifyNewData;
import com.appnomic.pipeline.spec.KeyValue;
import com.appnomic.pipeline.spec.checkpoint.CheckPointKVTrnasformer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class CPKVImplTransformer implements CheckPointKVTrnasformer<Data, String, TimeAmplifyNewData, Data>, Serializable {

   Map<String, Data> lastReceivedData = new HashMap<>();
    private static final long serialVersionUID = 1L;
    public static final Logger log = LoggerFactory.getLogger(CPKVImplTransformer.class);

    @Override
    public KeyValue<String, TimeAmplifyNewData> transform(Data data) {

        TimeAmplifyNewData timeboundData = new TimeAmplifyNewData(data.getStr(), data.getTimeStamp());
        KeyValue<String, TimeAmplifyNewData> keyValue = KeyValue.of(data.getStr(), timeboundData);
        lastReceivedData.put(data.getStr(), data);
        log.debug("Converted to keyValue : " +  data.getStr() +"  TimeBound Data  : " + timeboundData);
        return keyValue;
    }

    @Override
    public void init(Map<String, String> map) throws Exception {

    }


    @Override
    public List<Data> snapshotState() {
        log.debug("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ SanpShot state called ###############################, Size : " + lastReceivedData.size());
        return new ArrayList<>(lastReceivedData.values());
    }



    @Override
    public void restoreState(List<Data> list) {
        log.debug("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ Restore state called ###############################");
        list.forEach(data ->
                lastReceivedData.put(data.getStr(), data));
    }
}
