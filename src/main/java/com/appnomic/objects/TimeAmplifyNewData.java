package com.appnomic.objects;

/*
 *
 * Created by Akanksha on 14/8/18
 *
 */


import com.appnomic.pipeline.spec.Timestampify;

public class TimeAmplifyNewData implements Timestampify {
    private static final long serialVersionUID = 1L;
    String str;
    int value;
    long timeStamp;

    public TimeAmplifyNewData(String str, long timeStamp) {
        this.str = str;
        this.timeStamp = timeStamp;
    }

    public String getStr() {
        return str;
    }

    public void setStr(String str) {
        this.str = str;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }


    @Override
    public String toString() {
        return "Data{" +
                "str='" + str + '\'' +
                ", value=" + value +
                ", timeStamp=" + timeStamp +
                '}';
    }
    @Override
    public long getTimestamp() {
        return this.timeStamp;
    }
}
