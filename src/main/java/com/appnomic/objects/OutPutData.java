package com.appnomic.objects;

/*
 *
 * Created by Akanksha on 14/8/18
 *
 */


import java.io.Serializable;

public class OutPutData implements Serializable {
    String str;
    int value;
    int count;
    long timestamp;



    public OutPutData(int count, String key) {
        this.str = key;
        this.count = count;
    }

    public String getStr() {
        return str;
    }

    public void setStr(String str) {
        this.str = str;
    }

    public int getValue() {
        return value;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return "OutPutData{" +
                "str='" + str + '\'' +
                ", value=" + value +
                ", count=" + count +
                ", timestamp=" + timestamp +
                '}';
    }




}