package com.appnomic.objects;



import java.io.Serializable;


public class Data implements Serializable {
    String str;

    long timeStamp;

    public Data(){

    }

    public Data(String str, long timestamp){
        this.str = str;
        this.timeStamp = timestamp;
    }

    public String getStr() {
        return str;
    }

    public void setStr(String str) {
        this.str = str;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }


    @Override
    public String toString() {
        return "Data{" +
                "str='" + str + '\'' +
                ", timeStamp=" + timeStamp +
                '}';
    }
}
